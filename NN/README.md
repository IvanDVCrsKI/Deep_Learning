# NEURAL NETWORKS 

![cnn](img/thisiscnn.jpeg)




# required packages 

No mxnet ? Use autograd 

```
$ python3 -m pip install autograd
```




- activation functions (sig, tanh, relu) 
NOTE: use ReLU , sigmoid is obsolete !  
- gd /sgd
- dropout 
- weight decay 
- softmax 
- MLP 
- under / overfitting 
- forward / back prop 
- k- fold 
- training / testing 
- hyperparameters 
- cnn

