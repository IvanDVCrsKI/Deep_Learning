#!/usr/bin/env python

'''
==================================
TANH FUNKCIJA U NN
==================================
- graficki prikaz
- osnovna formula

DO NOT USE !!!
look as vanishing Gradient Problem - during back prop , gradients tend to be smaller and smaller - resulting in super slow learning 


f(x)= (1 - exp(-2x))/(1+exp(-2x))

instaliraj za izvode funkcija

'''

 
import matplotlib.pyplot as plt 
# za izvode 
import autograd.numpy as np 
from autograd import elementwise_grad as egrad



def tanh_fun(x):
    for i in x:
        p =  (1-np.exp(-2*x))/(1+np.exp(-2*x))
    return p 




    # for i in x:        
    #     p = (1-np.exp(-2x))/ (1+np.exp(-2x))    
    # return p
    
    
'''
Funkcija za izracunavanje tanh  
Parameters
----------
x -koristiti np.arrange()
      
Returns
-------
p - array 
koristiti za element wise izvod tj elementwise_grad
'''

# generate data 
x = np.arange(-10,10,0.1)


# activ function plot 
print(x)


# parametri grafika 
plt.grid(True)


plt.subplot(121).set_title('test')
plt.title("Tanh funkcija")
plt.plot(tanh_fun(x))

plt.subplot(122)
plt.title("Tanh izvod")
# izvod 
plt.plot(x,egrad(tanh_fun)(x))






plt.show()
