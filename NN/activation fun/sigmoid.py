#!/usr/bin/env python

'''
==================================
SIGMOID FUNKCIJA U NN
==================================
- graficki prikaz
- osnovna formula


NAPOMENA: sigmoid je zastareo princip koristiti 
DO NOT USE !!!
look as vanishing Gradient Problem - during back prop , gradients tend to be smaller and smaller - resulting in super slow learning 

f(x)= 1 /(1+exp(-x))

instaliraj za izvode funkcija
pip install autograd




'''

 
import matplotlib.pyplot as plt 

import autograd.numpy as np  # Thinly-wrapped numpy

from autograd import elementwise_grad as egrad



def sigmoid_fun(x):
     '''
        Funkcija za izracunavanje sigmoid  
        Parameters
        ----------
        x -koristiti np.arrange()
      
        Returns
        -------
        p - array 
        koristiti za element wise izvod 
   
        '''
    for i in x:
        p = 1/ (1+np.exp(-x))
    
    return p


# generate data 
x = np.arange(-10,10,0.1)


# activ function plot 
print(x)


# parametri grafika 
plt.grid(True)


plt.subplot(121).set_title('test')
plt.title("Sigmoid funkcija")
plt.plot(sigmoid_fun(x))

plt.subplot(122)
plt.title("Sigmoid izvod")
# izvod 
plt.plot(x,egrad(sigmoid_fun)(x))






plt.show()








