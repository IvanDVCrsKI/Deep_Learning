
'''
==================================
Linearna regresija  algoritam 
==================================
- osnovna formula 
- nacin treniranja 

----------------------------------------------------
Linearna regresija
----------------------------------------------------
Linearna regresija je jednoslojna neuronska mreza 


    O - output layer o1 
 / / \  \
O  O  O  O - input layer - or - feature dimension 
x1 x2 x3 xn


Formula 

y^ = w1*x1+w2*x2+...wn*xn+b 

w - weights
b -bias 
x - data points 
y^ - prediction 


y^ = wT * x +b 
wT - transponovana matrica vektora za weights 


loss function 

l2(Gauss)  = 1/2 * pow (y^ - y )
y^ - predictions / estimations /label
y - observations 


----------------------------------------------------
simple 6 PART process
----------------------------------------------------
1. Import what is needed from framework 
2. Get / clean /transform data 
3. Setup/Define model 
4. Train model 
5. Evaluate / optimize  
6. Make prediction 




'''



'''
----------------------------------------------------
1. Import what is needed from framework 
----------------------------------------------------
'''
#ploting
from matplotlib import pyplot as plt 

# gradient , nd array 
from mxnet import autograd,nd,gluon

# gluon data reading 
from mxnet.gluon import data as gluon_data

# model definition single layer neural network or linear regression 
from mxnet.gluon import nn

# model initalization 
from mxnet import init

# model loss function 
from mxnet.gluon import loss as gluon_loss

'''
----------------------------------------------------
2. Get / clean /transform data 
----------------------------------------------------
'''
nn_inputs = 2 
examples = 1000
bias = 5
weight = nd.array([2,-5])
features = nd.random.normal(shape=(examples,nn_inputs))

labels = nd.dot(features,weight)+bias

# minibatch size up to 32 - from reaserch paper 
batch_size = 10

# combine features and labels
dataset = gluon_data.ArrayDataset(features,labels)

# Seed mini batches randomly 
data_iterator = gluon_data.DataLoader(dataset,batch_size,shuffle=True)

for X,y in data_iterator:
    print(X,y)
    break


'''
----------------------------------------------------
3. Setup / Define model 
----------------------------------------------------
'''
# single layer nn 
model  = nn.Sequential()

# model initializer provides various methods for model parameter initalization ( linear regression - weight and bias )
model.initialize(init.Normal(sigma=0.01))

# loss function or Gauss 
loss_function = gluon_loss.L2Loss()

# optimization 
# use stahastic gradient descent  
# set hyperparameter learning rate 
# collect parameters from model 
trainer = gluon.Trainer(model.collect_params(), 'sgd', {'learning_rate': 0.03})

'''
----------------------------------------------------
4. Train model 
----------------------------------------------------
'''

epohe = 3 
for epohe in range(1,epohe+1):
    for X,y in data_iterator:
        with autograd.record():
            l=loss_function(model(X),y)
        l.backward()
        trainer.step(batch_size)
    l = loss_function(model(features),labels)
    print('epoha %d , loss: %f ' % (epohe,l.mean().asnumpy()))




'''
----------------------------------------------------
5. Evaluate   
----------------------------------------------------
'''

'''
----------------------------------------------------
6. Make prediction  
----------------------------------------------------
'''