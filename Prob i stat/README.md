**Verovatnoca i statistika**

![60p](img/60precent.jpeg?raw="True")


NOTE: velicina grafika zavisi od velicine ekrana

**Kockice**

- bacanje kockica "par" puta

![kockice](kockice_dist.png?raw=true "Title")

**Uniformna raspodela / distribucija**

- Random evaluation

![uni](unifomna_dist.png?raw=true "Title")

**Gauss**

- normalna raspodela / distribucija 

![gauss](normal_dist.png?raw=true "Title")

**Central limit theorem**

- sample za kockice tezi ka normalnoj raspodeli

![clt](clt_dist.png?raw=true "Title")
