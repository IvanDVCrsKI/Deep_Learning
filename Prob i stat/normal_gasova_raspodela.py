'''
==================================
Normalna ili Gaus / Laplasova 
==================================
- prikaz Gausove/normalne distribucije/raspodela brojeva 
- osnovna formula

p(x)= 1/sqrt(2*PI*varijansa) * exp(-(pow(x-mean_average),2)/(2*varijansa))
------------------------------------------------------------------------
mean_average (mu) from whole dataset
------------------------------------------------------------------------
formula:

mean_average=sum(Xn)/n

npr:
1+2+3 / 3 = 6/3 =2 
x1 = 1 ,x2 = 2, x3 = 3 ...xn
n=3 
mean = 2 
------------------------------------------------------------------------
variance - varijansa  
------------------------------------------------------------------------
formula:

variance  ( or pow (sigma,2) ) = sum ( pow(Xn - mean , 2)

npr:
x1 = 1 , x2 =2 , x3 =3 ,x4 =4
mean = (1+2+3+4)/4 = 10/4 = 2.5

variance = pow(x1-mean,2) +pow(x2-mean,2) +pow(x3-mean,2)+pow(x4-mean,2) / 3 
= (1-2.5)^2 + (2-2.5)^2 + (3-2.5)^2 + (4-2.5)^2/ 4 = -1.5^2 + -0.5^2 + 0.5^2 +1.5^2 = 
= 2.25 +0.25 + 0.25 2.25 = 5
variance = 5

------------------------------------------------------------------------
standardna devijacija - standard deviation (sigma ) za celu polulaciju / dataset 
------------------------------------------------------------------------
formula:

standard_deviation = sqrt(variance)

npr:
variance = 5

std_dev = sqrt(5) =2.23606....

------------------------------------------------------------------------
sample_standard deviation - from dataset take n amount of datapoints
------------------------------------------------------------------------
instead of /(n)  we divide (sum ( pow(Xn - sample_mean , 2)/(n)) we use /(n-1)
n-1 is Bassel's correction 

formula:

sample_standard_deviation = sqrt(sample_variance)
sample_variance = sum ( pow(Xn - sample_mean , 2)/(n-1)
sample_mean = sum(Xn)/n

'''
import numpy as np
from matplotlib import pyplot as plt 

import cProfile
# Copy paste za performanse 
# $ python -m cProfile -s tottime normal_gasova_raspodela.py
class Gauss():
    def __init__(self,mean):
        self.mean = np.mean(mean) 
     
    
    def prikaz_test(self):
        '''
        Funkcija za print postavljenih vrednosti u konstruktoru
        Parameters
        ----------
        none
      
        Returns
        -------
        none

        '''
            
        
        print(self.mean)

    def std_variance(self,x):
        # praracun standatdne varijacije pogledati u opisu formulu ^ 
        variance = 0
        count =0    
       
        for x_i in x: 
            count+=1
            variance = pow(x_i-self.mean,2)
            # print(x-self.mean)
      

        
        # print(sum(variance),x,sum(variance)/x)
        return variance/(count)

    def izracunaj_super_simple_gauss(self,x):
        '''
        Funkcija za izracunavanje pojednostavljenog oblika Gaussa 
        Parameters
        ----------
        x -koristiti np.arrange()
      
        Returns
        -------
        p,x - array 
        pristup podacima:
        p= f[0] - verovatnoca po simple Gaussu
        x =f[1] - niz brojeva 
        '''
        p = (1/np.sqrt(2*np.pi))*np.exp(-0.5*pow(x,2))
        return(p,x)
    
    def izracunaj_standard_gauss(self,x):
        '''
        Funkcija za izracunavanje Normalne raspodele tj Gaussa /Laplace 
        Parameters
        ----------
        x -koristiti np.arrange() - vrednosti niza x koje se prosledjuje direktno u funkciju za normalnu raspodelu 
        
        Ovo je STANDARDNA normalna distribucija tako da NE vazi n-1 Bassel's korekcija u okviru proracuna standardne devijacije 
        - u metodama se automatski proracunava varijacija i mean tj srednja vrednost 

      
        Returns
        -------
        p - array 
        p(x) za normalnu raspodelu za dat dataset x

      
        '''  

        std_deviation = np.sqrt(self.std_variance(x))      
        p = (1/np.sqrt(2*np.pi*pow(std_deviation,2))*np.exp(-1*(pow(x-self.std_variance(x),2)/2*pow(std_deviation,2))))
        return p 

# klase za Gauss 
Gauss_simple = Gauss(3)
Gauss_std = Gauss(np.arange(-10,10,0.01))


p_simple = Gauss_simple.izracunaj_super_simple_gauss(np.arange(-10,10,0.01))

p_normal = Gauss_std.izracunaj_standard_gauss(np.arange(-10,10,0.1))

#plot 
plt.plot(p_normal)
plt.title('Gauss / laplace normal distribution ')
plt.savefig('normal_dist.png', transparent=False) 
plt.show()