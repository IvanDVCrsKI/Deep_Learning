'''

=========================
Verovatnoca i statistika 
=========================

- osnovni termini 


'''


import mxnet as mx 
from mxnet import nd


import matplotlib.pyplot as plt

'''
Kockica ima 6 strana 
verovatnoca za neki od brojeva je 1/6 


'''

# prob vektor fill it with 1 
kocka_p =nd.ones(6)/6

# tenzor 1. reda duzine 6  (ili ti 1D ) 
print('tnz 1. reda: ',kocka_p.shape)



# baci kockicu ili ti sampleing 
roll_1K = nd.random.multinomial(kocka_p, shape=(1000))

print('tnz 2. reda: ', roll_1K.shape)


# napravi tenzor 2. reda  i popuni sa 0 
count = nd.zeros((1000,6))

# tenzor 2. reda 6x1000 RxC tj 2D 
print('tnz 1. reda: ', count.shape)


# napravi tenzor 0tog reda duzine 6 za prikaz totalne verovatnoce po broju 
total = nd.zeros(6)


# prodji kroz sve kolone u tenzoru 2 reda i za svaku kolonu dodaj 1 u total 
# tj ukupna vrednost za svaku od kolona koja predstavlja broj na kockici  
# roll_1K [6 row]x[1000 col] total  
for row,col in enumerate(roll_1K):
    # print('col',col)
    total[col.asscalar()] += 1
    #dodaj u kolonu brojeve koji su bobijeni tokom svake bacene kocke
    # iz matrice total 
    count[row,:] = total



# estimate ili estimacija je total/ brojem bacanja 
print('ESTIMACIJA ',total/1000)
print('Verovatnoca za jednu stranu kocke zaokruzeno na 3. decimalu ',round(1/6,3) )

# suma svih bacanja mora da je 1000
print('total sum',total.sum())

print('total count',count)


# normalizacija ili flattening // +1 za numericku stabilnost nan i inf 
x = nd.arange(1000).reshape((1000,1))+1

estimacija = count/x


print('11 bacanje' , estimacija[10,:])


# SUMA svih setimacija mora da je 1 
# tj sum(Pi) = 1 
print('suma verovatnoca je 1 ' , sum(estimacija[10,:]))

#velicina ceo ekran 
plt.figure('Verovatnoca i bacanja',figsize=(25,25))

# x osa broj bacanja , y osa strana kockice 
for i in range(6):
    plt.plot(estimacija[:,i].asnumpy(), label = str(i+1))
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)


plt.xlabel('Broj bacanja')
plt.ylabel('Verovatnoca za svaku od strana kockica ')
plt.grid(True)
plt.title('Odnos verovatnoca i broja bacanja')
plt.savefig('kockice_dist.png', transparent=False) 
plt.show()


