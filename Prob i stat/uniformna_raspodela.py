'''
==================================
SAMPLEING i uniformna raspodela
==================================
- prikaz uniformne/ravnomerna distribucije/raspodela brojeva 
- performanse sa @profile notacijom

'''

import matplotlib.pyplot as plt 
from mxnet import ndarray as nd
import random
import sys
import timeit
import cProfile



def uniformna_raspodela():
    '''
    funkcija koja sadrzi loop za prikaz uniformne raspodela 
    raspodela 10 grafika sa varijacijom od 10 do 100K
    od 0 do 100 random brojevi baceni od 10 od 100K puta 

    parametri:NONE
    return: NOONE 




    '''

    plt.figure("ravnomerna / unifiormna distribucija ", figsize=(8,8))
    plt.grid(True)

   

    # generate data
    # napravi tenzor 2. reda  i popuni sa 0 
    count = nd.zeros((101))

  
    # napravi N plotova 
    # na 10 ,100, 1000 napravi count izmedju 1 i 100

    broj_samplova  = [10,100,1000,10000,100000]
    boja_markera = ['red','blue','green','orange','pink','red']


    j = 0
    while True:   
        j+=1
        print('parametri',j) 
        plt.subplot(2,3,j)
        for i in range(broj_samplova[j-1]):
                    
            count[random.randint(0,100)]+=1
        plt.plot(count.asnumpy(), color=(boja_markera[j-1]),marker='o',markersize=2,linestyle='None')
        plt.title('N0 samples %s' % (broj_samplova[j-1]))
    

        if j ==len(broj_samplova):
            break      
        

    #print tenzor 
    print('y',count) 

    # save sliku  
    plt.savefig('unifomna_dist.png', transparent=False)  
   

 
# Poziv funkcije  
uniformna_raspodela()
# sys.exit("STHAAAAP")
