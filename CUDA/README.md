# CUDA UBUNTU 18.04 LTS INSTALLATION  


https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html

## Pre-installation Actions

```
$ lspci | grep -i nvidia
```
```
$ uname -m && cat /etc/*release 
```
- look for x86_64
```
$ gcc --version
```
```
$ sudo update-alternatives --config gcc
```
- update to version gcc 7 
```
$ uname -r
```
```
$ sudo apt-get install linux-headers-$(uname -r)
```
```
sudo dpkg -i cuda-repo-ubuntu1804-10-0-local-10.0.130-410.48_1.0-1_amd64.deb
```
```
sudo apt-key add /var/cuda-repo-<version>/7fa2af80.pub
```
```
sudo apt-get update
```
```
sudo apt-get install cuda
```
```
cat /proc/driver/nvidia/version
```
```
nvcc -V
```

# CUDA numba install
https://numba.pydata.org/numba-doc/latest/user/installing.html
