
'''
ZNACI NE MOZE JEDNOSTAVNIJE OD OVOGA 

'''
#standardni import za grafik
import matplotlib.pyplot as plt

#plot funkcija prosledj joj podatke

''' NAPOMENA
kad se array u plot-u inicializira kao 1D 
plt.plot([1,3,3,7]) 

x osa ima vrednosti pozicije elementa u nizu
x vrednosti [0,1,2,3]
y vrednosti [1,3,3,7]

'''
plt.plot([1,3,3,7,1,3,3,7,1,3,3,7])

#oznake na osama 
plt.xlabel('x osa')
plt.ylabel('y osa')

#prikazi grafik
plt.show()
