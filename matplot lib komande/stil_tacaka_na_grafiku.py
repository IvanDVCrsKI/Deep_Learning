'''

Stilovi prikazivanja tackaka na grafiku

'''
# standard plot import kao plt
import matplotlib.pyplot as plt

# import neophodan za help funkciju da prikaze sve opcije u help-u
import matplotlib as mat


# numpy za generisanje podataka as np
import numpy as np

#call for help  
# help(plt.plot)
# help(mat.markers)



'''
opis 
---------------------------
plt.plot(x,y,'op1' or op2)
---------------------------

op1 format - boja , oblik

r,g,b 
+,-,--,None,o,.,s,*,

op2 format tekstualni - color='ANYCOLOR' , marker = '' , linestyle = '', linewidth = , markersize=

marker = +,-,--,None,o,.,s,*,P ...za vise pogledati link
https://matplotlib.org/2.2.3/api/markers_api.html#module-matplotlib.markers
ili
help(mat.markers)


'''
# prikazi tacku preko tacke sa razlicitim bojama i izgledima

plt.plot([1],[1],color='red',marker='o',markersize=20)
#napomena red prikazivanja je bitan 
plt.plot([1],[1],'b+')

# generisani set podataka za x i y 
dic_xy_obj = {
'x_p':np.random.uniform(0,10,50),
'y_p':np.arange(0,50,1),
}


#provera dimenzionalnosti tenzora , iste dimenzije ili error 
# print(np.shape(dic_xy_obj['x_p']))
# print(np.shape(dic_xy_obj['y_p']))


# set vrednosti za siliziranje tacaka u vidu dict obj 
stil_dic ={
'marker':'s',
'markersize':20,
'color':[0, 0,0.1,0.1]  # hex  #0F0F0F ili rgba [0, 0,0.1,0.1] (izmedju 0 i 1) ili naziv red
}


plt.plot('x_p','y_p',color=stil_dic['color'],marker=stil_dic['marker'],markersize=stil_dic['markersize'], data=dic_xy_obj)




# oznake na osama
plt.xlabel('x osa')
plt.ylabel('y osa')


# prikazi
plt.show()

