'''

Stilovi prikazivanja tackaka na grafiku

'''
# standard plot import kao plt
import matplotlib.pyplot as plt


import numpy as np

'''

Podela figure na delove 
    11  |12   | 13
        |     |
------------------ 
     21 | 22  | 23
        |     |

subplot(nrows, ncols, index, **kwargs)

nrows - broj vrsta ceo broj 
ncols - broj kolona ceo broj 

PRVA 2 PARAMETRA JE VELICINA PODGRAFIKA 
npr 2x2 je grafik ima 4 mesta 
(221)
(222)
(223)
(224)

1x4 isto ima 4 mesta 
(141)
(142)
(143)
(144)

index - index figure npr  1. figura   


plt.subplot(123)
plt.subplot(1,200,3) za vece brojeve ( idealno za prikaz slika )


'''

# data generator
x_vektor = np.random.normal(0,10,100)

nazivi_grafika = ['alfa','beta','gama','delta']

plt.figure(1)
# 1 vrsta i 1 kolone figura broj 1 
plt.subplot(2,2,1)
plt.plot(x_vektor)
plt.title(nazivi_grafika[0])

# 1 vrsta i 4 kolone figura broj 2 
plt.subplot(2,2,2)
plt.hist(x_vektor)
plt.title(nazivi_grafika[1])




# plt.figure(2)
# 5 vrsta i 4 kolone figura broj 3 
plt.subplot(2,2,3)
plt.plot(np.sin(x_vektor))
plt.title(nazivi_grafika[2])

# 5 vrsta i 4 kolone figura broj 4 
plt.subplot(2,2,4)
plt.scatter(x_vektor,x_vektor)
plt.title(nazivi_grafika[3])

plt.show()