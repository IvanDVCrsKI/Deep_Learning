

'''
==================================
Super simple openCV image display  
==================================
- osnovna funkcionalnost prikaza slike u prozoru 

'''

import cv2
 
img = cv2.imread('img/ivvv.png')

print('Original Dimensions : ',img.shape) 

width = 300
height = 400
dim = (width, height)
 
# resize image
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA) 



edges = cv2.Canny(resized,300,400)
 
cv2.imshow("Edge Detected Image", edges)
 
cv2.waitKey(0) # waits until a key is pressed
cv2.destroyAllWindows() # destroys the window showing image




