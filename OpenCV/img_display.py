
'''
==================================
Super simple OpenCV image display
==================================
- platform linux  
- osnovna funkcionalnost prikaza slike u prozoru 


'''


import cv2
 
img = cv2.imread('img/ivvv.png')
 
cv2.imshow('sample image',img)
 
cv2.waitKey(0) # waits until a key is pressed
cv2.destroyAllWindows() # destroys the window showing image

# use ESC to close window 