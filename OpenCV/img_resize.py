'''
==================================
OpenCV img resize 
==================================
- manipulacija slike 
- procentualno smanjivanje  

'''


import cv2
 
img = cv2.imread('img/ivvv.png', cv2.IMREAD_UNCHANGED)
 
print('Original Dimensions : ',img.shape)
 
scale_percent = 10 # ex. 10% od originalne velicine 
width = int(img.shape[1] * scale_percent / 100)
height = int(img.shape[0] * scale_percent / 100)
dim = (width, height)
# resize image
resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
 
print('Resized Dimensions : ',resized.shape)
 
cv2.imshow("Resized image", resized)



cv2.waitKey(0)
cv2.destroyAllWindows()